package DAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;

import util.HibernateUtil;

public abstract class DAOGeneric<T> {

	protected Session session;
	protected Class<T> entityClass;
	
	public DAOGeneric(Session s, Class<T> e) {
		session = s;
		entityClass = e;
	}

	
	public T find(int id) {
		return (T) session.get(entityClass, id);
	}
	
	public void save(T entity) {
		session.getTransaction().begin();
		session.saveOrUpdate(entity);
		session.getTransaction().commit();
	}
	
	public void delete(T entity) {
		session.getTransaction().begin();
		session.delete(entity);
		session.getTransaction().commit();
	}
	
	public long count() {
		Criteria criteria = session.createCriteria(entityClass);
		criteria.setProjection(Projections.rowCount());
		return (long) criteria.uniqueResult();
	}
	
	public ArrayList<T> findAll(){
		ArrayList<T> list = new ArrayList<T>();
		list = (ArrayList<T>) session.createQuery("FROM "+entityClass.getName()).list();
		
		
		return list;
	}
	
}
